---
title: "Basic setup"
linkTitle: "Basic setup"
weight: 1
description: >
  Basic gitlab user usage
---

## Cloning the upstream repository

If the user only wants to build and install the latest development versions (without suggesting any changes to them), the upstream source repositories can be checked out using `git` and the HTTPS paths for each of them. These paths are given in the dialog displayed after clicking the **Clone** button.

![Clone dialog](/images/gitlab-mm-clone-dialog.png)

The git clone commands for each of these repositories would be:

```
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/libmbim.git
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/libqmi.git
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/ModemManager.git
```

## Creating a new user account

In order to access the basic functionality of the gitlab instance, like reporting new issues or commenting on already existing ones, a [new user account]((https://gitlab.freedesktop.org/users/sign_up)) should be registered in the gitlab instance.

## Reporting new issues

The user can browse existing issues or report new ones in the project specific **Issues** page:

 - ModemManager: https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues
 - libqmi: https://gitlab.freedesktop.org/mobile-broadband/libqmi/-/issues
 - libmbim: https://gitlab.freedesktop.org/mobile-broadband/libmbim/-/issues
 - libqrtr-glib: https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib/-/issues

Once the issue is reported, the maintainers may request to add additional explanations, or provide debug logs with detailed protocol information.
