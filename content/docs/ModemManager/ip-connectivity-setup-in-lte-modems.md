---
title: "IP connectivity setup in LTE modems"
linkTitle: "IP connectivity setup in LTE modems"
weight: 6
description: >
  An outline of the different IP settings a ModemManager bearer object may expose, and the expected actions the host would require to perform to set up the IP addressing in the modem.
---

## Introduction

Setting up an ethernet network connection on a host is a very common process that many people know about. This process usually involves configuring the network interface with certain predefined IPv4 or IPv6 addresses, netmask, link MTU, default route gateway, and DNS servers; or otherwise getting all those settings dynamically from the network itself, e.g. through IPv4 DHCP or IPv6 SLAAC. These simple flows are possible because the ethernet network interfaces are devices with no signaling plane (no control protocol) to manage them, where the user plane (data path) is ready to be set up and used without any additional configuration.

LTE modems, and any other cellular modem for that matter, require the host to run certain control commands before the data path can be considered ready. Even if the modem exposes a network interface in the system, this interface is not functional until the modem is actually connected to the operator network.

In systems based on the Linux kernel, the ModemManager system daemon takes care of the signaling plane between host and modem. The user of ModemManager is usually another daemon like [NetworkManager](https://networkmanager.dev), openwrt [netifd](https://openwrt.org/docs/techref/netifd) or ChromeOS [shill](https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/shill/README.md), which are responsible for the overall network connection that needs to be set up. This is process can be summarized in the following steps:
 * The network management daemon asks ModemManager to get the modem connected with certain settings.
 * ModemManager uses control commands to attach and connect the modem to the network. This communication is exclusively done via the signaling plane between host and LTE modem (e.g. through QMI, MBIM or AT commands).
 * The LTE modem negotiates IP addresses with the LTE network.
  * This communication is first done via the signaling plane between LTE modem and LTE network (e.g. through [NAS](https://en.wikipedia.org/wiki/Non-access_stratum) messages transferred between the modem and the network [MME](https://en.wikipedia.org/wiki/System_Architecture_Evolution#MME_(Mobility_Management_Entity))).
  * Once the signaling plane communication is finished, additional steps in the newly created user plane may be required (e.g. to run IPv6 SLAAC with the network [PGW](https://en.wikipedia.org/wiki/System_Architecture_Evolution#PGW_(Packet_Data_Network_Gateway))).
 * The LTE modem provides the IP settings to use to ModemManager, again via the signaling plane between host and LTE modem.
 * ModemManager reports back the IP settings to use, and the network management daemon uses them to configure the network interface exposed by the modem, effectively setting up the data path in the host.

![Signaling and user planes](/images/signaling-and-user-planes.png)

The following sections outline with more detail how the IP settings are reported by the LTE network to the modem, and how the modem exposes them for the host to use.


## Signaling between modem and network

### Initial EPS default bearer establishment

In GPRS, WCDMA and other technologies before LTE, there was a clear separation between the user equipment registering in the network (the network attach procedure) and setting up a packet data connection. LTE does away with this separation, combining both things together to provide the user with a data connection as soon as possible.

**The IP address allocation procedure between the LTE modem and the network is done at the time of the network registration**, using the APN settings configured for the initial EPS default bearer. This does not mean the whole data path setup with the network is done at network registration time, as that may also depend on the host needs. Even if IP settings for the modem have already been assigned, the host may still not consider the modem as connected, and the corresponding network interface may even be still down.

The initial EPS default bearer setup procedure, which includes the agreement of which IP settings to use, is exclusively performed via signaling exchanges in the control plane between modem and network. Without going into detail of all the messages transmitted between modem and network during the network attach process in LTE, the key details regarding IP addressing are considered in the following transactions:
 * The modem requests which **IP type** to use (IPv4-only, IPv6-only or dual stack IPv4v6) in the **PDN Connectivity Request** message. In this same request, the modem will also request IPv4 and/or IPv6 DNS servers and the link MTU.
 * The network asks the modem for the APN to use during the attach process through an ESM Information Request message.
 * The modem replies with the **APN** to use in the ESM Information Response message.
 * The network uses the APN to select which packet gateway (PGW) should handle the IP session of the modem, and performs the bearer setup procedure with it.
 * The network replies with an **Activate Default Bearer Context Request** that includes information provided by the PGW:
   * The **negotiated IP type** (IPv4-only, IPv6-only or dual stack IPv4v6). This may be different to the requested one, if e.g. IPv4v6 is requested but IPv6-only supported by the network.
   * The **assigned IPv4 address**, if IPv4-only or IPv4v6 was negotiated.
   * A **temporary IPv6 Interface ID**, if IPv6-only or IPv4v6 was negotiated.
   * The IPv4 and/or IPv6 addresses of the DNS servers.
   * The link MTU.
 * The modem replies with an Activate Default Bearer Context Accept message.

### IPv4 addressing ###

Assuming IPv4-only or IPv4v6 was negotiated, after the signaling sequence has been completed **the modem has been already assigned an IPv4 address**.

At this point, this IP address is known at least by the modem itself and by the network PGW, and therefore the modem may already be able to communicate with the Internet using IPv4. The IPv4 data path is fully set up between LTE modem and LTE network, but the IP address is not yet known by the host.

### IPv6 addressing ###

Assuming IPv6-only or IPv4v6 was negotiated, after the signaling sequence has been completed **the modem does not have a fully valid IPv6 address yet**.
The global IPv6 address must be acquired through the IPv6 address auto-configuration (SLAAC) procedure.

## IPv6 addressing with SLAAC ##

In order to get a valid global IPv6 address, the IPv6 SLAAC procedure should be run once the user plane has been established via signaling:
 * An IPv6 Router Solicitation (RS) message is sent to the network, using as source address the link-local address built with the explicit temporary IPv6 Interface ID provided via signaling. If a different link-local address is used, the network may silently drop the message, as it would not be considered part of the just established user plane.
 * The network responds with a IPv6 Router Advertisement (RA) message containing the **IPv6 prefix** and optionally the **IPv6 link MTU**. DNS servers are not returned via this message.

The global IPv6 address to use can be constructed using the IPv6 prefix and any Interface ID, which can be changed at any time for privacy reasons. There is no requirement to use the previous temporary Interface ID retrieved via signaling.

Worth noting, there is no need to run IPv6 neighbor discovery procedure to avoid duplicated addresses. The PGW in the network ensures the IPv6 prefix is unique for each established user data bearer.

The LTE standard states that the User Equipment (UE) is responsible for running the SLAAC procedure after the signaling sequence. However, in most setups, the UE is divided into a host system and a modem, as outlined in this document. **The standard does not specify whether SLAAC should be run by the host or by the modem**, so different modem implementations will behave differently.

### Modem SLAAC ###

Some modems (e.g. all the ones based on a Qualcomm chipset) will run SLAAC themselves (**modem SLAAC**) as soon as the signaling sequence is finished. In this case, the IPv6 data path is fully set up between LTE modem and LTE network, but the IP address is not yet known by the host.

![Modem SLAAC](/images/modem-slaac.png)

### Host SLAAC ###

Some modems (e.g. all the ones based on the Intel XMM chipset) will not run SLAAC themselves, and will require the host to run it (**host SLAAC**). The IPv6 data path will not be ready between LTE modem and LTE network until the host finalizes the procedure.

Given that the PGW does not return DNS servers in this scenario, the host should obtain them from the modem through control commands in the signaling plane.

![Host SLAAC](/images/host-slaac.png)

### Host SLAAC after modem SLAAC ###

The host may decide to ignore the global IPv6 settings reported by the modem, and run SLAAC again.

This new SLAAC procedure would be run between host and modem exclusively; i.e. the modem performs IPv6 tethering and assigns additional IPv6 Interface IDs to the host over the IPv6 prefix already agreed with the network. During this procedure, the Router Advertisement built by the modem may also include DNS servers and link MTU size.

This approach is usually supported by modems that run SLAAC themselves, although **technically there is really no guarantee**.

![Host SLAAC after modem SLAAC](/images/host-slaac-after-modem-slaac.png)

## Signaling between host and modem ##

### IP settings reported in the ModemManager Bearer objects ###

Once a data connection is established through ModemManager, the Bearer object exposed in the bus associated with the connection will include IP settings for IPv4, IPv6 or both, depending on the IP type requested by the user and the IP type agreed with the network. The fields expected in each of the IP settings (IPv4 or IPv6) are the same, but may have slightly different meanings.

#### IP method ####

The IP method field in the IP settings specify how the host should configure the network interface in the modem device. This field has little to do with how the modem acquires the IP addressing from the network.

 * **PPP**: the modem requests the host to establish a PPP session over the serial port specified as data interface. This method is used in RS232 modems, or in USB modems with limited capabilities (e.g. modems that don’t expose a network interface). For the purpose of this document, this case can be ignored.
 * **Static**: the modem requests the host to use the IP settings reported by the modem via control messages.
 * **DHCP**: the modem requests the host to use dynamic IP addressing setup, i.e. DHCP for IPv4 or SLAAC for IPv6.

#### Address ####

The address field includes the IP address to be configured in the network interface. This may be the final IP address to use (e.g. IPv4 and/or global IPv6) or a temporary address (e.g. link-local IPv6 address to run host SLAAC).

#### Netmask and gateway IP address ####

The netmask and gateway IP addresses are never negotiated with the serving network. These two fields are exclusively used in the connection between host and modem, and they provide a way for the host to configure its IP addressing and routing stack to correctly communicate with the modem.

For the purpose of the data connection, the gateway IP address is even optional. Very old modems would not provide any, so the host would need to set up a network route without a gateway (i.e. bound to the network interface instead).

Not good enough modems may decide to use very wide subnets (e.g. netmask /8), easily colliding with other subnets in other interfaces (e.g. especially if the IP address returned by the network is e.g. in a common private network like 10.x.x.x). The most common case, though, is good modems choosing a very minimal subnet that contains just a few IP addresses, including the network provided IP address along with a gateway IP address.

The netmask and gateway IP addresses are not expected when the host is responsible for running SLAAC, although some modems may provide them. They can be ignored, if that is the case.

#### DNS servers ####

ModemManager may report up to three DNS servers for each IP type, but it is usually just two or one, depending on the network.

These DNS servers are reported exclusively via PCOs in the signaling plane between modem and network, retrieved with control commands in the signaling plane between host and modem, and then propagated directly to the fields in the Bearer IP settings.

### MTU ####

For IPv4 (e.g. when connecting IPv4-only or IPv4v6), the link size is reported via PCO in the signaling plane between modem and network, retrieved with control commands in the signaling plane between host and modem, and then propagated directly to the field in the Bearer IP settings.

For IPv6 (e.g. when connecting IPv6-only or IPv4v6), the link size is reported via SLAAC RA in the user plane between modem and network.
 * In the modem SLAAC scenario, the MTU value will be retrieved with control commands in the signaling plane between host and modem, and then propagated directly to the field in the Bearer IP settings.
 * In the host SLAAC scenario, the MTU value will be provided by the network directly to the host.

In reality, the MTU value should be the same for both IPv4 and IPv6, but modems may report different values if they come from different sources (e.g. one value could be provided by the preconfigured carrier settings in the modem while the other one is provided by the network itself). When different values are reported, the smallest one should be used.

### Network interface configuration ###

ModemManager does not perform the network interface configuration in the host, as it is exclusively in charge of communicating in the signaling plane between host and modem. The network manager process in use in the system will collect the IP settings exposed in the ModemManager Bearer object, and use them to configure the network interface accordingly.

#### IPv4 static settings ####

If the IPv4 settings in the Bearer object report **“static”** method and contain an IPv4 address and DNS servers, the settings may be applied right away in the network interface. Netmask and gateway are also usually provided, which allow the host system to easily set up a route through the modem to the Internet.

This method is used if there is an explicit control message between host and modem to query the IP settings, or when there is no DHCP server running inside the modem.

In particular, QMI modems running in raw-ip mode (without ethernet headers in the link layer) will by default be requested to use static IP addressing; not because the DHCP server in the modem doesn’t work, but because not all DHCP clients in Linux can work with network interfaces without ethernet headers (e.g. dhclient can’t).

#### IPv4 dynamic settings ####

If the IPv4 settings in the Bearer object report **“dhcp”** method, the host must run a DHCP operation in the network interface to obtain the IP address and other settings.

This method is used if there is no known control message to query the IP settings, or if we know the DHCP server running inside the modem is functional.

In particular, QMI modems running in 802.3 mode (with ethernet headers in the link layer) will by default be requested to use DHCP.

#### IPv6 static settings ####

If the IPv6 settings in the Bearer object report **“static”** method and contain a global IPv6 address and DNS servers (and optionally link MTU), the settings may be applied right away in the network interface. Netmask and gateway are also usually provided, which allow the host system to easily set up a route through the modem to the Internet.

This method is used if the modem has performed SLAAC by itself (modem SLAAC) and we already have a global IPv6 address and a gateway IP address to use.

The host is not required to run SLAAC in this case. If it does run SLAAC, it’s up to the modem implementation to support it (host SLAAC on top of modem SLAAC).

#### IPv6 dynamic settings ####

If the IPv6 settings in the Bearer object report **“dhcp”** method, the host must run SLAAC in the network interface to obtain the global IP address (and optionally link MTU).

In this scenario, the IP settings may also include the link-local IPv6 address that should be configured in the network interface before the host SLAAC procedure is run. If a different link-local address is used (e.g. because the modem does not provide it to the host, or because the host does not set it before starting the procedure) the network may silently drop the message.

This method is used if the modem does not run SLAAC itself (host SLAAC).

DNS servers must be read from the Bearer IP settings, because they are not provided in the SLAAC Router Advertisement.

## Examples ##

### Fibocom FM101 attach and data setup: IPv4v6, host SLAAC on top of modem SLAAC ###

#### Signaling between modem and network ####

The contents of the **PDN connectivity request** message sent by the Qualcomm-based Fibocom FM101 during the network attach are as follows:

![FM101 PDN connectivity request](/images/fm101-pdn-connectivity-request.png)

In the above message we can see:
 * Dual stack **PDN type IPv4v6** is requested.
 * **Both IPv4 and IPv6 DNS servers** are requested via PCOs.
 * The **IPv4 link MTU** is requested via PCO.

Once the initial default EPS bearer is established in the network, the Activate Default Bearer Request is sent from the network to the modem, with the following contents:

![FM101 Activate Default Bearer Context Request](/images/fm101-activate-default-bearer-context-request.png)

In the above message we can see:
 * The **10.45.0.3** IPv4 address is assigned to the modem.
 * The **::0:0:0:2** IPv6 interface ID is assigned to the modem.
 * Primary and secondary IPv4 DNS servers are reported via PCOs, **8.8.8.8** and **8.8.4.4**.
 * Primary and secondary IPv6 DNS servers are reported via PCOs, **2001:4860:4860:8888** and **2001:4860:4860:8844**.
 * IPv4 link MTU of **1400** octets is reported via PCO.

#### Modem SLAAC ####

The FM101 is a Qualcomm based modem, and therefore it will run SLAAC itself. The modem sends the following IPv6 Router Solicitation message:

![FM101 Router Solicitation](/images/fm101-router-solicitation.png)

As expected, the RS message above is sent from the IPv6 link-local address built using the network-specified interface ID value (**fe80::2**) and sent to the link-local all routers multicast address (ff02::2).

The network replies with an IPv6 Router Advertisement message as follows:

![FM101 Router Solicitation](/images/fm101-router-advertisement.png)

The previous message shows the final global IPv6 prefix the modem is supposed to use (**2001:db8:cafe:1::**). In the case above, the network did not return IPv6 MTU in the SLAAC RS message, which is probably an error in the network itself.

#### Signaling between host and modem ####

All the previous messages happen during the network attach process, so by the time ModemManager starts a data session (e.g. using MBIM in this case), the IP settings to use are already known by the modem, there are no further interactions with the network in the signaling layer.

On the ModemManager side, the user requests a Simple.Connect() operation via DBus, asking to connect to the same APN that was used during the attach process, and the modem is able to directly reply back with the IP settings to use.

The information exposed by ModemManager in the Bearer object looks as follows:

```
# mmcli -b 1
  ------------------------------------
  General            |           path: /org/freedesktop/ModemManager1/Bearer/1
                     |           type: default
  ------------------------------------
  Status             |      connected: yes
                     |      suspended: no
                     |    multiplexed: no
                     |      interface: wwan0
                     |     ip timeout: 20
  ------------------------------------
  Properties         |       apn type: default
                     |        roaming: forbidden
                     |        ip type: ipv4v6
  ------------------------------------
  IPv4 configuration |         method: static
                     |        address: 10.45.0.3
                     |         prefix: 30
                     |        gateway: 10.45.0.5
                     |            dns: 8.8.8.8, 8.8.4.4
                     |            mtu: 1400
   ------------------------------------
  IPv6 configuration |         method: static
                     |        address: 2001:db8:cafe:1:754e:73c3:176c:ff47
                     |         prefix: 64
                     |        gateway: 2001:db8:cafe:1:c47b:a1d:6d44:6bbd
                     |            dns: 2001:4860:4860::8888, 2001:4860:4860::8844
                     |            mtu: 1400
```

All the settings reported by the network are included in the IPv4 configuration and IPv6 configuration sections.

Additionally,
 * The modem chose a very small subnet with prefix /30 to contain the network assigned IP address (10.45.0.3) along with the fake gateway IP address (10.45.0.5).
 * The modem chose an interface ID of 754e:73c3:176c:ff47.
 * The modem chose an IPv6 gateway address with the same prefix as the one assigned by the network, 2001:db8:cafe:1:c47b:a1d:6d44:6bbd.

#### Host SLAAC on top of modem SLAAC ####

At this point, the host already has all the IPv4 and IPv6 settings it needs, so nothing else would be necessary. Still, the network management system in the host may choose to run SLAAC again as in this example.

The host sends the following Router Solicitation:

![FM101 Host Router Solicitation](/images/fm101-host-router-solicitation.png)

The modem replies with a Router Advertisement as follows.

![FM101 Host Router Advertisement](/images/fm101-host-router-advertisement.png)

The previous host SLAAC operation is performed exclusively between the host and the modem. There is no SLAAC procedure being done with the network itself, because the IPv6 prefix has already been agreed.

It is the modem itself replying to the Router Solicitation sent by the host with a Router Advertisement, in an IPv6 tethering kind of setup, that includes:
 * IPv6 prefix, same as returned by the network.
 * Link size,  same as returned by the network.
 * IPv6 DNS servers, the same ones returned by the network.

Note how the RA received in this case, unlike the one sent by the network, contains DNS information and MTU size.

### Fibocom L850 attach and data setup: IPv4v6 and host SLAAC ###

#### Signaling between modem and network ####

The contents of the **PDN connectivity request** message sent by the L850 during the network attach are as follows:

![L850 PDN connectivity request](/images/l850-pdn-connectivity-request.png)

In the above message we can see:
 * Dual stack **PDN type IPv4v6** is requested.
 * **IPv4 DNS servers** are requested via the legacy PPP IP Control Protocol PCO.
 * **IPv6 DNS servers** are requested via PCO.

In this case, no link MTU is requested via PCO, which means this modem does not by default support network specific link MTU sizes.

Once the initial default EPS bearer is established in the network, the **Activate Default Bearer Request** is sent from the network to the modem, with the following contents:

![L850 Activate Default Bearer Context Request](/images/l850-activate-default-bearer-context-request.png)

In the above message we can see:
 * The **10.45.0.4** IPv4 address is assigned to the modem.
 * The **::0:0:0:2** IPv6 interface ID is assigned to the modem.
 * Primary and secondary IPv4 DNS servers are reported via PCOs, **8.8.8.8** and **8.8.4.4**.
 * Primary and secondary IPv6 DNS servers are reported via PCOs, **2001:4860:4860:8888** and **2001:4860:4860:8844**.

#### Signaling between host and modem ####

All the previous messages happen during the network attach process, so by the time ModemManager starts a data session (e.g. using MBIM in this case), at least the IPv4 settings to use are already known by the modem. The L850 is an Intel XMM based modem that isn’t capable of running SLAAC itself, and therefore IPv6 settings are not fully ready yet.

On the ModemManager side, the user requests a Simple.Connect() operation via DBus, asking to connect to the same APN that was used during the attach process. The modem is able to directly reply back with the IPv4 settings to use, and the link-local IPv6 address that it needs to use to run host SLAAC.

The information exposed by ModemManager in the Bearer object looks as follows:

```
# mmcli -b 1
  ------------------------------------
  General            |           path: /org/freedesktop/ModemManager1/Bearer/1
                     |           type: default
  ------------------------------------
  Status             |      connected: yes
                     |      suspended: no
                     |    multiplexed: yes
                     |      interface: mbimmux0.1
                     |     ip timeout: 20
  ------------------------------------
  Properties         |            apn: internet
                     |       apn type: default
                     |        roaming: forbidden
                     |        ip type: ipv4v6
  ------------------------------------
  IPv4 configuration |         method: static
                     |        address: 10.45.0.4
                     |         prefix: 24
                     |        gateway: 10.45.0.1
                     |            dns: 8.8.8.8, 8.8.4.4
                     |            mtu: 1500
  ------------------------------------
  IPv6 configuration |         method: dhcp
                     |        address: fe80::2
                     |         prefix: 120
                     |        gateway: fe80::1
                     |            dns: 2001:4860:4860::8888, 2001:4860:4860::8844
                     |            mtu: 1500
```

All the settings reported by the network are included in the IPv4 configuration and IPv6 configuration sections.

Additionally,
 * The modem chose a relatively small subnet with prefix /24 to contain the network assigned IP address (10.45.0.4) along with the fake gateway IP address (10.45.0.1).
 * The modem built a link-local address with the network assigned IPv6 Interface ID (fe80::2).
 * The modem reported IPv6 prefix and IPv6 gateway, even if technically these are not required for anything, as the host needs to run SLAAC anyway.
 * The modem reports a fake MTU of 1500 that is probably hardcoded in the firmware. There was no MTU negotiation with the network, so this is somewhat expected.

#### Host SLAAC ####

Once the IPv6 link-local address is known by the network management system, the host SLAAC procedure can run. The host sends the following IPv6 Router Solicitation message:

![L850 Router Solicitation](/images/l850-router-solicitation.png)

As expected, the RS message above is sent from the IPv6 link-local address built using the network-specified interface ID value (**fe80::2**) and sent to the link-local all routers multicast address (ff02::2).

The network replies with an IPv6 Router Advertisement message as follows:

![L850 Router Solicitation](/images/l850-router-advertisement.png)

The previous message shows the final global IPv6 prefix the modem is supposed to use (**2001:db8:cafe:1::**). Given that the SLAAC is performed against the network PGW, no DNS information is reported in this message.
