---
title: "WWAN device types"
linkTitle: "WWAN device types"
weight: 4
description: >
  Types of WWAN devices and how they are exposed in the system
---

## Types of devices and how they are exposed in the system

`ModemManager` can interact with almost every type of mobile broadband modem that exposes at least one control port. Devices without control ports (e.g. some USB dongles that expose a net port exclusively) aren’t supported by `ModemManager`.

### USB devices

USB devices are the most common type of devices, and come in very different form factors. USB dongles that can be connected to the physical USB port of the computer are one type, but modules connected in miniPCIe or M.2 slots are also very common, especially in embedded systems and laptops.

![USB device types 1](/images/wwan-device-types-usb1.png)

![USB device types 2](/images/wwan-device-types-usb2.png)

A device enumerated in the USB bus is able to expose in the system multiple ports, so there is no limitation on the number of control ports or data ports that can be found. USB modems with multiple control ports are very common, e.g. allowing different operations to be run in parallel in the different ports; but modems with multiple data ports also exist, e.g. in order to allow connections to multiple APNs separately without the need of complex multiplexing setups

The types of ports that are expected on this type of device are:
 * Serial ports (e.g. `ttyUSB0`, `ttyACM0`…).
 * Network ports (e.g. `wwan0`, `usb0`, `eth0`…).
 * QMI or MBIM control ports (e.g. `cdc-wdm0`…)
 * WWAN subsystem QMI or MBIM control ports (e.g. `wwan0mbim0`, `wwan0qmi0`).

#### Devices that require USB mode switching

Sometimes the USB devices boot by default on a non-modem USB layout, e.g. exposing themselves as a USB disk. This was very common with USB dongles (e.g. from Huawei, ZTE…) that required special drivers to be installed in the Windows operating system; the USB disk exposed by the dongle would provide the tool to install that driver.

In Linux based systems the users expect the devices to work without requiring any additional driver installation, and to achieve that there are tools like usb-modeswitch that are able to automatically switch the devices into a valid modem USB layout without any manual user intervention.

#### Devices with multiple USB configurations

There are devices (e.g. a lot of Foxconn or Huawei modules) that expose multiple USB configurations by default, each of them providing a different layout of USB interfaces. One configuration could for example include a MBIM control port and a network interface, another configuration could include an AT port and a NCM network interface, and so on.

The devices will by default be exposed by the kernel in the USB configuration that best matches a generic layout. E.g. if a device exposes both MBIM and QMI layouts in different configurations, the MBIM one will always be preferred because it’s considered more generic than the QMI one. Sometimes this is not enough, though, as devices with NCM and MBIM layouts are both generic, and the kernel may choose the NCM over the MBIM one by default, while the MBIM one is really preferred in general. In order to cope with these cases, the usb-modeswitch tool allows to automatically select the MBIM configuration regardless of what others are available in the USB device.

#### Devices with support for multiple layouts via control port configuration

There are devices (e.g. a lot of Sierra Wireless modules) that allow selecting the USB layout in order to for example include additional AT ports on an otherwise bare QMI and net port layout, or even completely change the layout from a QMI based one to a MBIM based one.

### RS232 devices

RS232 devices are still used in certain setups due to their simplicity, but they are mostly phased out for USB modems that are able to expose multiple ports via a single USB connection.

There are two main ways to have these devices integrated into the system: either using a real physical RS232 connection or otherwise using a USB to RS232 adapter/converter. In both those cases, the devices will end up exposing a single serial port in the system, which `ModemManager` will use first for control (AT) and then for data (PPP).

![RS232 device types](/images/wwan-device-types-rs232.png)

Unlike USB modems, the RS232 modems require very explicit settings agreed beforehand with the host in order to properly talk to them (e.g. baudrate, number of stop bits, flow control settings…). Any misconfiguration in any of these settings (either in the modem itself or in the host) could make the communication impossible.

The types of ports that are expected on this type of device are:
 * Platform serial ports (e.g. `ttyS0`…).
 * USB serial ports (e.g. `ttyUSB0`…).

### PCI devices

PCIe devices are much more complex and advanced than most USB devices, because they require a tighter integration with the host system. Among other things, this allows them to have much higher data throughputs during an ongoing connection than what’s achievable with USB 3.0 and USB 2.0.

Unlike the USB case, PCIe devices do not expose multiple optional configurations or layouts. The device has preconfigured in the firmware the list of channels to be accessible by the kernel drivers. There may be some configurability supported, but it is not as common as with USB devices.

These devices come in the same form factor as miniPCIe or M.2 USB modems, but using the PCI bus instead of the USB bus. A device working in PCIe mode will not work as a USB device, and a device working in USB mode will not work as a PCIe device. Some manufacturers allow changing the PCIe/USB mode via software (e.g. with proprietary AT commands) and some others allow changing it via hardware (e.g. by changing the voltage level in an input GPIO).

PCIe devices are integrated in the upstream Linux kernel using the WWAN subsystem introduced in kernel 5.13. This subsystem also allows the kernel to explicitly define which control protocol is expected to be used in the port, so there is no need for custom port type hints in `ModemManager`.

The types of ports that are expected on this type of device are:
* Network ports (e.g. `wwan0`).
* WWAN subsystem AT, QMI or MBIM control ports (e.g. `wwan0mbim0`, `wwan0qmi0`, `wwan0at0`…).

![PCI device types](/images/wwan-device-types-pci.png)

### Qualcomm SoCs

The case of Qualcomm SoCs is quite different from the usual modem management case because there is no external device connected to the host, the modem is integrated in the host system itself, and therefore requires different ways to communicate with it.

Modem control operations through nodes in the QRTR bus are supported, as well as through character devices exposed by the WWAN subsystem built on top of the rpmsg subsystem. Depending on the capabilities of the SoC one method or the other will be used; older SoCs have rpmsg support exclusively, others have both rpmsg and QRTR (but the latter not for modem management), and newer SoCs have full support for QRTR (including modem management).

The network interface support in Qualcomm SoCs is also a bit different, because it is required to bind a given data connection to a specific network interface explicitly. In SoCs using the `bam-dmux` kernel driver, there are usually a fixed number of network interfaces already exposed in the system, while in newer `ipa` driver based setups it is required to instantiate new virtual network interfaces when a connection is being brought up.

![Qualcomm SoC device types](/images/wwan-device-types-qcom-soc.png)

The types of ports that are expected on this type of device are:
 * Network ports (e.g. `rmnet_ipa0`).
 * WWAN subsystem AT or QMI control ports (e.g. `wwan0qmi0`, `wwan0at0`, in QC MSM8916).
 * QRTR nodes accessible via the QRTR bus (e.g. in QC 7cG3). These control ports do not expose any character device in /dev, they are only accessible via AF_QIPCRTR sockets.

## Hints to investigate device layouts

### USB devices

The devices that are exposed in the USB bus can be inspected quickly using the tree view of the lsusb command which includes information about the available interfaces in the currently selected configuration.
```
$ lsusb --tree
```

Once the device to investigate has been identified, the lsusb command can also be used to query the whole set of details for the USB device, including which are all the USB configurations it has (even if only one of them is currently selected).
```
$ lsusb -v -d <vid:pid>
```

Using the usb-devices command is also useful as it provides information of which kernel driver is actually managing each of the ports of the device. In this case, though, the command only shows information about the current USB device configuration and layout.
```
$ usb-devices
```

The USB devices and each of their control and data ports will get entries under `sysfs`, and the device-wide `sysfs` entry is what `ModemManager` uses to bind all ports of a given modem together in the same logical object.

### PCI devices

The lspci command can be used in a similar way to the lsusb one but for PCI devices instead of USB devices.
```
$ lspci -t
$ lspci -v
```

### QRTR capable devices

For Qualcomm devices that are accessible through the QRTR subsystem, several command line tools are available in [Bjorn Andersson’s github](https://github.com/andersson/qrtr) repository:
```
$ qrtr-lookup
```

### Udev device traversal

Running udevadm to investigate how udev sees a given port is always very handy, especially when writing new udev rules.

Querying all information for a specific device can be done as follows:
```
$ sudo udevadm info -p /sys/class/<subsystem>/<name>
```

If new udev rules were added, they should be visible in the output of the previous command.

Querying all the attributes for a device and all its parents (if any) can be done as follows:
```
$ sudo udevadm info -a /sys/class/<subsystem>/<name>
```

The previous query will dump multiple lines with different attributes that can be used as-is in custom udev rules.
